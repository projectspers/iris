import os
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point, Polygon, MultiPolygon

polygon_iris = gpd.read_file(r"/CONTOURS-IRIS.shp")

polygon_iris = polygon_iris.sort_values(by=["CODE_IRIS"], ascending=True)
polygon_iris = polygon_iris.rename(columns={'geometry': 'CONTOUR_IRIS', 'TYP_IRIS': 'TYPE_IRIS'})

polygon_iris.to_csv('contours_iris.csv', index = False)
