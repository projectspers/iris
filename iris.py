import os
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point, Polygon, MultiPolygon

code_iris = pd.read_csv(r"reference_IRIS_geo2021.csv")
print(code_iris)

polygon_iris = gpd.read_file(r"CONTOURS-IRIS.shp")
polygon_iris.head()
polygon_iris.plot()

polygon_iris.describe()

merge_iris = code_iris.merge(polygon_iris, left_on='CODE_IRIS', right_on='CODE_IRIS')
merge_iris.head()
del merge_iris['TYP_IRIS_y']

print(merge_iris.geometry.dtypes)
merge_iris.to_csv('geometrie_iris.csv')
